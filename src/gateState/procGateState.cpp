#include <iostream>

#include "procGateState.h"
#include "openGateState.h"
#include "closedGateState.h"

state_return ProcGateState::enter(){
  std::cout << "calling enter on state process, state is staying the same.\n";
  return std::nullopt;
}
state_return ProcGateState::pay(){
  std::cout << "calling pay on state process, state is staying the same.\n";
  return std::nullopt;
}
state_return ProcGateState::payOk(){
  std::cout << "calling payOk on state process, changing state to open.\n";
  return std::make_unique<OpenGateState>();
}
state_return ProcGateState::payFailed(){
  std::cout << "calling payFailed on state process, changing state to closed.\n";
  return std::make_unique<ClosedGateState>();
}
