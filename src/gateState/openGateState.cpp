#include <iostream>

#include "openGateState.h"
#include "closedGateState.h"

state_return OpenGateState::enter(){
  std::cout << "calling enter on state open, changing state to closed.\n";
  return std::make_unique<ClosedGateState>();
}
state_return OpenGateState::pay(){
  std::cout << "calling pay on state open, state is staying the same.\n";
  return std::nullopt;
}
state_return OpenGateState::payOk(){
  std::cout << "calling payOk on state open, state is staying the same.\n";
  return std::nullopt;
}
state_return OpenGateState::payFailed(){
  std::cout << "calling payFailed on state open, state is staying the same.\n";
  return std::nullopt;
}
