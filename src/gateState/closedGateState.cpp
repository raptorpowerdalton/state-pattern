#include <iostream>

#include "closedGateState.h"
#include "procGateState.h"

state_return ClosedGateState::enter(){
  std::cout << "calling enter on state closed, state is staying the same.\n";
  return std::nullopt;
}
state_return ClosedGateState::pay(){
  std::cout << "calling pay on state closed, changing state to process.\n";
  return std::make_unique<ProcGateState>();
}
state_return ClosedGateState::payOk(){
  std::cout << "calling payOk on state closed, state is staying the same.\n";
  return std::nullopt;
}
state_return ClosedGateState::payFailed(){
  std::cout << "calling payFailed on state closed, state is staying the same.\n";
  return std::nullopt;
}
