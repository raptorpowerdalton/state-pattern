#ifndef GATE_H
#define GATE_H

#include <memory>
#include <optional>

#include "gateState.h"
#include "openGateState.h"
#include "closedGateState.h"
#include "procGateState.h"

class Gate{
public:
  Gate(GateState* state){
    this->state.reset(state);
  }
  ~Gate(){

  }
  void enter(){
    if(auto ret = state->enter()){
      state = std::move(*ret);
    }
  }
  void pay(){
    if(auto ret = state->pay()){
      state = std::move(*ret);
    }
  }
  void payOk(){
    if(auto ret = state->payOk()){
      state = std::move(*ret);
    }
  }
  void payFailed(){
    if(auto ret = state->payFailed()){
      state = std::move(*ret);
    }
  }
private:
  std::unique_ptr<GateState> state;
};



#endif
