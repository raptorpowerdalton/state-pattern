#ifndef CLOSEDGATESTATE_H
#define CLOSEDGATESTATE_H

#include "gateState.h"

class ClosedGateState : public GateState{
public:

  state_return enter();
  state_return pay();
  state_return payOk();
  state_return payFailed();
};

#endif
