#ifndef OPENGATESTATE_H
#define OPENGATESTATE_H

#include "gateState.h"

class OpenGateState : public GateState{
public:

  state_return enter();
  state_return pay();
  state_return payOk();
  state_return payFailed();
};

#endif
