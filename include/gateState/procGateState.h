#ifndef PROCGATESTATE_H
#define PROCGATESTATE_H

#include "gateState.h"

class ProcGateState : public GateState{
public:

  state_return enter();
  state_return pay();
  state_return payOk();
  state_return payFailed();
};

#endif
