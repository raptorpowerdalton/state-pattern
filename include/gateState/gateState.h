#ifndef GATESTATE_H
#define GATESTATE_H

#include <optional>
#include <memory>

class GateState;
using state_return = std::optional<std::unique_ptr<GateState>>;

class GateState{
public:
  virtual ~GateState(){

  }
  virtual state_return enter() = 0;
  virtual state_return pay() = 0;
  virtual state_return payOk() = 0;
  virtual state_return payFailed() = 0;
};


#endif
